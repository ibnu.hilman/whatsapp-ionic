/**
 * Created by hilmanibnu on 3/7/17.
 */
import { MongoObservable } from 'meteor-rxjs';
import { Chat } from '../models';

export const Chats = new MongoObservable.Collection<Chat>('chats');
