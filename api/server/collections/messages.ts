/**
 * Created by hilmanibnu on 3/7/17.
 */
import { MongoObservable } from 'meteor-rxjs';
import { Message } from '../models';

export const Messages = new MongoObservable.Collection<Message>('messages');
